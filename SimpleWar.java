public class SimpleWar{
	public static void main(String[] args){	
		Deck drawPile = new Deck();
		drawPile.shuffle();
		int player1Points = 0;
		int player2points = 0;
		
		//System.out.println(drawPile.drawTopCard());
		//System.out.println(drawPile.drawTopCard());
		//System.out.println(drawPile.drawTopCard().calculateScore());
	
		//While loop to start the game and make it run until the deck runs out of cards
		while(drawPile.length() > 0){
			Card Card1 = drawPile.drawTopCard();
			Card Card2 = drawPile.drawTopCard();
		
			//Print the first card
			System.out.println(Card1);
			//Calculate it's score
			System.out.println(Card1.calculateScore());
			//Print the second card
			System.out.println(Card2);
			//Calculate it's score			
			System.out.println(Card2.calculateScore());
		
			System.out.println("Player 1 Points " + player1Points);
			System.out.println("Player 2 Points " + player2points);
		
			//If/Else statement to compare the values of the two cards and allocate the points
			if(Card1.calculateScore() > Card2.calculateScore()){
				System.out.println("Player 1 Wins");
				player1Points++;
			}
			else{
				System.out.println("Player 2 Wins");
				player2points++;
			}
			System.out.println("Player 1 Points " + player1Points);
			System.out.println("Player 2 Points " + player2points);
		}
		
		
		//Check the player who won after going through the entire deck
		if(player1Points > player2points){
			System.out.println("Player 1 Wins, Congragulations");
		}
		else if(player2points > player1Points){
			System.out.println("Player 2 Wins, Congragulations");
		}
		else{
			System.out.println("It was a tie, Congragulations to the both of you");
		}
	}
}