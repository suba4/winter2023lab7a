import java.util.Random;
public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	//Done
	public Deck(){
		this.cards = new Card[52];
		this.rng = new Random();
		this.numberOfCards = 52;
		String[] suits = {"Hearts", "Diamonds", "Clubs", "Spades"};
		String[] values = {"Ace", "Two" , "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten" , "Jack" , "Queen", "King" };
		
		
		int cardIndex = 0;
		
		for(int suitsInput = 0; suitsInput < suits.length; suitsInput++){	
			for(int valuesInput = 0; valuesInput < values.length; valuesInput++){
				cards[cardIndex] = new Card(suits[suitsInput],values[valuesInput]);
				cardIndex++;					
			}	
			
		}	
	}
	//Done
	public int length(){
		return this.numberOfCards;
	}
	
	//Done
	public Card drawTopCard(){
		this.numberOfCards--;
		return cards[this.numberOfCards];
	}
	
	//Done
	public String toString(){
		String allCards = "The remaining cards are : " + "\n";
		for (int i = 0; i<numberOfCards; i++){
			allCards += cards[i];
		}
		return allCards;
	}
	
	//Done
	public void shuffle(){
		for (int i = 0; i < this.cards.length; i++) {
			int randNum = i + rng.nextInt(this.cards.length - i);
        	Card test = this.cards[i];
        	this.cards[i] = this.cards[randNum];
        	this.cards[randNum] = test;
		}
	}
	
}