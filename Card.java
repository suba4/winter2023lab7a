public class Card{
	private String suit;
	private String value;
	
	public Card(String suit, String value){
		this.suit = suit;
		this.value = value;
	}	
	public String getSuit(){
		return this.suit;
	}
	
	public String getValue(){
		return this.value;
	}
	public String toString(){
		return this.value + " " +  "of" + " " + this.suit + " " + "\n";
	}
	public double calculateScore(){
		//Set the rankValue to 0 to be able to add onto it later on
		double rankValue = 0.0;
		//Copy string array values in order to use it in the for loop
		String[] values = {"Ace", "Two" , "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten" , "Jack" , "Queen", "King" };
		
		//for loop to set the values
		for(int i = 0; i < values.length ; i++){
			if(value.equals(values[i])){
				//add the array position + 1;
				rankValue += i + 1;
			}
		}
		
		//hard code values for suits
		if(this.suit.equals("Hearts")){
			rankValue += 0.4;
		}
		else if(this.suit.equals("Spades")){
			rankValue += 0.3;
		}
		else if(this.suit.equals("Diamonds")){
			rankValue += 0.2;
		}
		else{
			rankValue += 0.1;
		}
		return rankValue;
	}
}